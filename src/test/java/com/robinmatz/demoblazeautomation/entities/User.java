package com.robinmatz.demoblazeautomation.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class User {
    private String username;
    private String password;

    public static class UserBuilder {
        private String username = "username-" + UUID.randomUUID();
        private String password = "abcdefg";

        private UserBuilder() {
        }

        public static UserBuilder aUser() {
            return new UserBuilder();
        }

        public UserBuilder withUsername(String username) {
            this.username = username;
            return this;
        }

        public User build() {
            return new User(username, password);
        }
    }
}
