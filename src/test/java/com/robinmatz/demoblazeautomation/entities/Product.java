package com.robinmatz.demoblazeautomation.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Product {
    private String title;
    private String price;
    private String description;
    private String url;
}
