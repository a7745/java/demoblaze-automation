package com.robinmatz.demoblazeautomation;

import com.aventstack.extentreports.Status;
import com.robinmatz.demoblazeautomation.entities.User;
import com.robinmatz.demoblazeautomation.infrastructure.TestProperties;
import com.robinmatz.demoblazeautomation.infrastructure.VisualLogger;
import com.robinmatz.demoblazeautomation.pageobjects.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.UUID;

import static com.robinmatz.demoblazeautomation.entities.User.UserBuilder.aUser;

public class DemoblazeClient {

    private final WebDriver driver;

    public DemoblazeClient(WebDriver driver) {
        this.driver = driver;
        driver.get(TestProperties.getUrl());
        driver.manage().window().maximize();
    }

    public RegistrationForm goToRegistrationForm() {
        WebElement signUpLink = driver.findElement(By.id("signin2"));
        signUpLink.click();

        return new RegistrationForm(driver);
    }

    public LoginForm goToLoginForm() {
        WebElement logInLink = driver.findElement(By.id("login2"));
        logInLink.click();

        return new LoginForm(driver);
    }

    public LatestProducts latestProducts() {
        return new LatestProducts(driver);

    }

    public ShoppingCart goToShoppingCart() {
        WebElement cartLink = driver.findElement(By.id("cartur"));
        cartLink.click();

        return new ShoppingCart(driver);
    }

    public LoggedInUser registerNewUserAndLogin() {
        try (VisualLogger logger = new VisualLogger()) {
            String username = "username-" + UUID.randomUUID();
            User user = aUser()
                    .withUsername(username)
                    .build();

            logger.log(Status.INFO, "Registering user " + user);
            RegistrationForm registrationForm = goToRegistrationForm();
            registrationForm.registerUser(user.getUsername(), user.getPassword());

            LoginForm loginForm = goToLoginForm();
            return loginForm.loginUser(user.getUsername(), user.getPassword());
        }
    }
}
