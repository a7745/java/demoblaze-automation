package com.robinmatz.demoblazeautomation.tests;

import com.robinmatz.demoblazeautomation.DemoblazeClient;
import com.robinmatz.demoblazeautomation.entities.Product;
import com.robinmatz.demoblazeautomation.infrastructure.TestBase;
import com.robinmatz.demoblazeautomation.pageobjects.*;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SanityTests extends TestBase {
    @Test
    void loggedInUserCanPurchaseItem() {
        DemoblazeClient client = openNewDemoblazeClient();
        LoggedInUser user = client.registerNewUserAndLogin();

        Product product = client.latestProducts().first();
        ProductPage productPage = user.selectProduct(product);
        productPage.addProductToCart();
        ShoppingCart cart = client.goToShoppingCart();
        PurchaseForm purchaseForm = cart.placeOrder();
        PurchaseSuccessAlert alert = purchaseForm.fillDetailsAndPurchase();

        assertThat(alert.getId()).matches("\\d+");
    }

    private DemoblazeClient openNewDemoblazeClient() {
        return new DemoblazeClient(driver);
    }
}
