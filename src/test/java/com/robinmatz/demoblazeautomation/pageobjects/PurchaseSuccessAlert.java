package com.robinmatz.demoblazeautomation.pageobjects;

import com.robinmatz.demoblazeautomation.infrastructure.BasePage;
import lombok.AccessLevel;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

@Getter
public class PurchaseSuccessAlert extends BasePage {
    private final String id;
    private final String amount;
    private final String cardNumber;
    private final String name;
    private final String date;
    @Getter(AccessLevel.PRIVATE)
    private final WebElement container;

    public PurchaseSuccessAlert(WebDriver driver) {
        super(driver);
        container = driver.findElement(By.className("lead"));
        id = readId();
        amount = readAmount();
        cardNumber = readCardNumber();
        name = readName();
        date = readDate();
    }

    private String readId() {
        return getRegexMatchAndRemovePrefix("Id: \\d+", "Id: ");
    }

    private String readAmount() {
        return getRegexMatchAndRemovePrefix("Amount: \\d+", "Amount: ");
    }

    private String readCardNumber() {
        return getRegexMatchAndRemovePrefix("Card Number: \\d+", "Card Number: ");
    }

    private String readName() {
        return getRegexMatchAndRemovePrefix("Name: .+", "Name: ");
    }

    private String readDate() {
        return getRegexMatchAndRemovePrefix("Date: [0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}", "Date: ");
    }

    private String getRegexMatch(String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(container.getText());

        String match = null;
        while (matcher.find()) {
            match = matcher.group();
        }

        assertThat(match)
                .withFailMessage("No matches for pattern '%s' were found.", regex)
                .isNotNull();

        return match;
    }

    private String getRegexMatchAndRemovePrefix(String regex, String prefix) {
        String match = getRegexMatch(regex);
        return match.replace(prefix, "");
    }
}
