package com.robinmatz.demoblazeautomation.pageobjects;

import com.robinmatz.demoblazeautomation.entities.Product;
import com.robinmatz.demoblazeautomation.infrastructure.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class LatestProducts extends BasePage {
    private final List<Product> latestProducts;

    public LatestProducts(WebDriver driver) {
        super(driver);
        latestProducts = getLatestProducts();
    }

    public Product first() {
        return latestProducts.get(0);
    }

    private List<Product> getLatestProducts() {
        String cssSelector = "#tbodyid .card";
        wait.until(webDriver -> webDriver.findElements(By.cssSelector(cssSelector)).size() == 9);
        List<WebElement> productElements = driver.findElements(By.cssSelector(cssSelector));

        List<Product> products = new ArrayList<>();

        for (WebElement element : productElements) {
            String title = element.findElement(By.className("card-title")).getText();
            String price = element.findElement(By.cssSelector(".card-block h5")).getText();
            String description = element.findElement(By.cssSelector("#article.card-text")).getText();
            String url = element.findElement(By.tagName("a")).getAttribute("href");

            products.add(new Product(title, price, description, url));
        }

        return products;
    }
}
