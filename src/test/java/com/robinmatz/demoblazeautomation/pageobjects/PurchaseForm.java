package com.robinmatz.demoblazeautomation.pageobjects;

import com.aventstack.extentreports.Status;
import com.robinmatz.demoblazeautomation.infrastructure.BasePage;
import com.robinmatz.demoblazeautomation.infrastructure.VisualLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Calendar;

public class PurchaseForm extends BasePage {
    public PurchaseForm(WebDriver driver) {
        super(driver);
    }

    public PurchaseSuccessAlert fillDetailsAndPurchase() {
        try (VisualLogger logger = new VisualLogger()) {
            logger.log(Status.INFO, "Filling purchase information");

            WebElement nameInput = driver.findElement(By.id("name"));
            nameInput.sendKeys("Dummy name");

            WebElement countryInput = driver.findElement(By.id("country"));
            countryInput.sendKeys("Dummy country");

            WebElement cityInput = driver.findElement(By.id("city"));
            cityInput.sendKeys("Dummy city");

            WebElement creditCardInput = driver.findElement(By.id("card"));
            creditCardInput.sendKeys("1234123412341234");

            WebElement monthInput = driver.findElement(By.id("month"));
            String currentMonth = String.valueOf(Calendar.getInstance().get(Calendar.MONTH));
            monthInput.sendKeys(currentMonth);

            WebElement yearInput = driver.findElement(By.id("year"));
            String currentYear = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
            yearInput.sendKeys(currentYear);

            WebElement purchaseButton = driver.findElement(By.cssSelector("#orderModal button[onclick]"));
            purchaseButton.click();

            return new PurchaseSuccessAlert(driver);
        }
    }
}
