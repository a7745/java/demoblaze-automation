package com.robinmatz.demoblazeautomation.pageobjects;

import com.aventstack.extentreports.Status;
import com.robinmatz.demoblazeautomation.infrastructure.BasePage;
import com.robinmatz.demoblazeautomation.infrastructure.VisualLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ProductPage extends BasePage {
    public ProductPage(WebDriver driver) {
        super(driver);
    }

    public void addProductToCart() {
        try (VisualLogger logger = new VisualLogger()) {
            logger.log(Status.INFO, "Adding product to cart");
            WebElement addToCartButton = driver.findElement(By.cssSelector(".product-content .row a[onclick]"));
            addToCartButton.click();
            acceptAlert();
        }
    }
}
