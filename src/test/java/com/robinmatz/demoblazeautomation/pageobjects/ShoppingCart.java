package com.robinmatz.demoblazeautomation.pageobjects;

import com.aventstack.extentreports.Status;
import com.robinmatz.demoblazeautomation.infrastructure.BasePage;
import com.robinmatz.demoblazeautomation.infrastructure.VisualLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ShoppingCart extends BasePage {
    public ShoppingCart(WebDriver driver) {
        super(driver);
    }

    public PurchaseForm placeOrder() {
        try (VisualLogger logger = new VisualLogger()) {
            logger.log(Status.INFO, "Placing order");

            WebElement pageWrapper = driver.findElement(By.id("page-wrapper"));
            WebElement placeOrderButton = pageWrapper.findElement(By.tagName("button"));
            placeOrderButton.click();

            return new PurchaseForm(driver);
        }
    }
}
