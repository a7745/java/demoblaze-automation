package com.robinmatz.demoblazeautomation.pageobjects;

import com.aventstack.extentreports.Status;
import com.robinmatz.demoblazeautomation.infrastructure.BasePage;
import com.robinmatz.demoblazeautomation.infrastructure.VisualLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class RegistrationForm extends BasePage {

    public RegistrationForm(WebDriver driver) {
        super(driver);
    }

    public void registerUser(String username, String password) {
        try (VisualLogger logger = new VisualLogger()) {
            logger.log(Status.INFO, "Registering user " + username);

            WebElement usernameInput = driver.findElement(By.id("sign-username"));
            usernameInput.sendKeys(username);

            WebElement passwordInput = driver.findElement(By.id("sign-password"));
            passwordInput.sendKeys(password);

            WebElement signUpButton = driver.findElement(By.cssSelector("#signInModal .modal-footer [onclick]"));
            signUpButton.click();

            acceptAlert();
        }
    }
}
