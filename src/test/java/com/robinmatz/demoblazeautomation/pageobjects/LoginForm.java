package com.robinmatz.demoblazeautomation.pageobjects;

import com.aventstack.extentreports.Status;
import com.robinmatz.demoblazeautomation.infrastructure.BasePage;
import com.robinmatz.demoblazeautomation.infrastructure.VisualLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginForm extends BasePage {

    public LoginForm(WebDriver driver) {
        super(driver);
    }

    public LoggedInUser loginUser(String username, String password) {
        try (VisualLogger logger = new VisualLogger()) {
            logger.log(Status.INFO, "Logging in with username " + username);

            WebElement usernameInput = driver.findElement(By.id("loginusername"));
            usernameInput.sendKeys(username);

            WebElement passwordInput = driver.findElement(By.id("loginpassword"));
            passwordInput.sendKeys(password);

            WebElement loginButton = driver.findElement(By.cssSelector("#logInModal .modal-footer button[onclick]"));
            loginButton.click();

            wait.until(ExpectedConditions.refreshed(
                    ExpectedConditions.textToBePresentInElementLocated(By.id("nameofuser"), "Welcome " + username)
            ));

            return new LoggedInUser(driver);
        }
    }
}
