package com.robinmatz.demoblazeautomation.pageobjects;

import com.robinmatz.demoblazeautomation.entities.Product;
import com.robinmatz.demoblazeautomation.infrastructure.BasePage;
import org.openqa.selenium.WebDriver;

public class LoggedInUser extends BasePage {
    public LoggedInUser(WebDriver driver) {
        super(driver);
    }

    public ProductPage selectProduct(Product product) {
        driver.get(product.getUrl());
        return new ProductPage(driver);
    }
}
