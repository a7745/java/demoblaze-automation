package com.robinmatz.demoblazeautomation.infrastructure;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.service.ExtentTestManager;
import com.aventstack.extentreports.testng.listener.ExtentITestListenerClassAdapter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringDecorator;
import org.openqa.selenium.support.events.WebDriverListener;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;

import java.io.IOException;

import static com.robinmatz.demoblazeautomation.infrastructure.ScreenshotUtils.takeScreenshotAndReturnPath;

@Listeners({ExtentITestListenerClassAdapter.class})
public class TestBase {
    protected WebDriver driver;

    @BeforeMethod
    protected void setUp(ITestResult result) {
        ExtentTestManager.createMethod(result, true);
        WebDriver original = WebDriverFactory.createDriver(
                TestProperties.getDriverType(),
                TestProperties.isRemote()
        );
        WebDriverListener listener = new WebDriverListenerImpl(original);
        driver = new EventFiringDecorator<>(listener).decorate(original);
    }

    @AfterMethod
    protected void tearDown(ITestResult result) throws IOException {
        if (result.getStatus() == ITestResult.FAILURE) {
            String path = takeScreenshotAndReturnPath(driver);
            String htmlSource = getHtmlSource();
            ExtentTestManager.getTest(result).fail(
                    MediaEntityBuilder.createScreenCaptureFromPath(path).build());
            ExtentTestManager.getTest(result).fail("<pre>" + htmlSource + "</pre>");
        }

        if (driver != null)
            driver.quit();
    }

    private String getHtmlSource() {
        String source = driver.getPageSource();
        return source
                .replace("<", "&lt")
                .replace(">", "&gt");
    }
}
