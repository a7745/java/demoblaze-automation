package com.robinmatz.demoblazeautomation.infrastructure;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.util.Arrays;

public class WebDriverFactory {

    private static final String TEST_RESOURCES_DIR =
            System.getProperty("user.dir") + "/src/test/resources/";
    private static final String CHROMEDRIVER_PROPERTY = "webdriver.chrome.driver";
    private static final String FIREFOX_PROPERTY = "webdriver.firefox.driver";

    public static WebDriver createDriver(DriverType driverType, boolean isRemote) throws IllegalArgumentException {
        if (isRemote)
            return getRemoteWebDriver(driverType);
        else
            return getLocalWebDriver(driverType);

    }

    private static RemoteWebDriver getLocalWebDriver(DriverType driverType) {
        switch (driverType) {
            case CHROME -> {
                setPropertyFor(DriverType.CHROME);
                return new ChromeDriver();
            }
            case FIREFOX -> {
                setPropertyFor(DriverType.FIREFOX);
                return new FirefoxDriver();
            }
            default -> throw new IllegalArgumentException(
                    "Driver %s is not available. Available drivers are %s"
                            .formatted(driverType, Arrays.toString(DriverType.values()))
            );
        }
    }

    private static void setPropertyFor(DriverType driverType) {
        switch (driverType) {
            case CHROME:
                if (isWindows())
                    System.setProperty(CHROMEDRIVER_PROPERTY, TEST_RESOURCES_DIR + "chromedriver.exe");
                else
                    System.setProperty(CHROMEDRIVER_PROPERTY, TEST_RESOURCES_DIR + "chromedriver");

            case FIREFOX:
                if (isWindows())
                    System.setProperty(FIREFOX_PROPERTY, TEST_RESOURCES_DIR + "geckodriver.exe");
                else
                    System.setProperty(FIREFOX_PROPERTY, TEST_RESOURCES_DIR + "geckodriver");
        }
    }

    private static boolean isWindows() {
        return System.getProperty("os.name").startsWith("Windows");
    }

    private static WebDriver getRemoteWebDriver(DriverType driverType) {
        switch (driverType) {
            case CHROME -> {
                ChromeOptions options = new ChromeOptions();
                return new RemoteWebDriver(
                        TestProperties.getGridUrl(),
                        options
                );
            }

            case FIREFOX -> {
                FirefoxOptions options = new FirefoxOptions();
                return new RemoteWebDriver(
                        TestProperties.getGridUrl(),
                        options
                );
            }

            default -> throw new IllegalArgumentException(
                    "Driver %s is not available. Available drivers are %s"
                            .formatted(driverType, Arrays.toString(DriverType.values()))
            );
        }
    }
}
