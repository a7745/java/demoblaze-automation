package com.robinmatz.demoblazeautomation.infrastructure;

public enum DriverType {
    CHROME("chrome"),
    FIREFOX("firefox");

    public final String value;

    DriverType(String value) {
        this.value = value;
    }
}
