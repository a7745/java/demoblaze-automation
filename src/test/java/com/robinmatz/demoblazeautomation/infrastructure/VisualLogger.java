package com.robinmatz.demoblazeautomation.infrastructure;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.service.ExtentTestManager;

public class VisualLogger implements AutoCloseable {

    private static int indentation = -1;

    public VisualLogger() {
        startSection();
    }

    private void startSection() {
        indentation++;
    }

    private void endSection() {
        indentation--;
    }

    public void log(Status status, String details) {
        ExtentTestManager.getTest().log(status,
                "<span style='display: inline-block; padding-left: " + indentation + "cm;'>"
                        + details + "</span>");
    }

    @Override
    public void close() {
        endSection();
    }
}
