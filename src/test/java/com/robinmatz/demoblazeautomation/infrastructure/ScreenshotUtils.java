package com.robinmatz.demoblazeautomation.infrastructure;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScreenshotUtils {

    // Using same OUTPUT_PATH as set in extent.properties
    // [extent.reporter.spark.out]
    private static final String OUTPUT_PATH = "target/extent-reports/";

    public static String takeScreenshotAndReturnPath(WebDriver driver) throws IOException {
        File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String fileName = createFileName();
        String destFilePath = OUTPUT_PATH + fileName;
        File destFile = new File(destFilePath);
        FileUtils.copyFile(file, destFile);
        return fileName;
    }

    private static String createFileName() {
        SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd-HHmmss");
        String dateString = f.format(new Date());
        return "screenshot-" + dateString + ".png";
    }
}
