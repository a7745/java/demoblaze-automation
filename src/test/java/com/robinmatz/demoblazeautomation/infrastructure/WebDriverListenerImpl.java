package com.robinmatz.demoblazeautomation.infrastructure;

import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverListener;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.Arrays;

public class WebDriverListenerImpl implements WebDriverListener {
    private final WebDriverWait wait;

    public WebDriverListenerImpl(WebDriver driver) {
        wait = new WebDriverWait(
                driver,
                Duration.ofSeconds(TestProperties.getDriverWaitTime())
        );
    }

    @Override
    public void beforeGet(WebDriver driver, String url) {
        try (VisualLogger logger = new VisualLogger()) {
            logger.log(Status.INFO, "Navigating to '" + url + "'");
        }
    }

    @Override
    public void beforeFindElement(WebDriver driver, By locator) {
        wait.until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    @Override
    public void beforeSendKeys(WebElement element, CharSequence... keysToSend) {
        try (VisualLogger logger = new VisualLogger()) {
            wait.until(ExpectedConditions.visibilityOf(element));
            logger.log(
                    Status.INFO,
                    "Sending keys '" + Arrays.toString(keysToSend) + "' to element '" + element + "'"
            );
        }
    }

    @Override
    public void beforeClick(WebElement element) {
        try (VisualLogger logger = new VisualLogger()) {
            wait.until(ExpectedConditions.visibilityOf(element));
            logger.log(Status.INFO, "Clicking element '" + element + "'");
        }
    }
}
