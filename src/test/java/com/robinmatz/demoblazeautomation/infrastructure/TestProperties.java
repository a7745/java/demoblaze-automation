package com.robinmatz.demoblazeautomation.infrastructure;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

public class TestProperties {
    private static final String PROPERTY_FILE_NAME = "test.properties";
    private static final String PROPERTY_FILE_PATH =
            System.getProperty("user.dir") + "/src/test/resources/" + PROPERTY_FILE_NAME;
    private static final String PROPERTY_TEMPLATE_FILE_PATH = PROPERTY_FILE_PATH + ".template";
    private static Properties properties;

    static {
        loadData();
    }

    private static void loadData() throws IllegalStateException {
        properties = new Properties();
        try {
            properties.load(new FileInputStream(PROPERTY_FILE_PATH));
        } catch (IOException e) {
            throw new IllegalStateException(
                    "Property file %s was not found. Please create one from %s"
                            .formatted(PROPERTY_FILE_PATH, PROPERTY_TEMPLATE_FILE_PATH)
            );
        }
    }

    public static String getUrl() {
        return getProperty("url");
    }

    public static DriverType getDriverType() throws RuntimeException {
        String driverType = getProperty("driver.type");

        try {
            return DriverType.valueOf(driverType.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalStateException("%s\nPlease change the property [driver.type] in your test.properties."
                    .formatted(e.getMessage())
            );
        }
    }

    public static int getDriverWaitTime() {
        String driverWaitTime = getProperty("driver.wait_time");
        return Integer.parseInt(driverWaitTime);
    }

    public static boolean isRemote() {
        String isRemote = getProperty("selenium.remote");
        return Boolean.parseBoolean(isRemote);
    }

    public static URL getGridUrl() {
        String property = "selenium.grid_url";
        try {
            return new URL(getProperty(property));
        } catch (MalformedURLException e) {
            throw new RuntimeException("URL definded in %s is invalid.".formatted(property));
        }
    }

    private static String getProperty(String key) throws IllegalStateException {
        String value = properties.getProperty(key);
        if (value == null) {
            throw new IllegalStateException("Property [%s] is not defined in your %s".formatted(key, PROPERTY_FILE_NAME));
        }
        return value;
    }
}
