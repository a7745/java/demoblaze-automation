package com.robinmatz.demoblazeautomation;

@SuppressWarnings("unused")
public class NotImplementedException extends RuntimeException {
    public NotImplementedException() {
        super("Method is not implemented");
    }
}
